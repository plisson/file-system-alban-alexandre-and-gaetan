  BasicOS project report:

Some of thoses programes must be executed with SUDO PRIVILEDGES. 

Thoses characters cannot be used in stores files: \1 and \2 (ascii 1 and 2)

The objective of this project is to create a file system with some fundamental functions.
  We have chosen to set the parameters of our file system in te following way:

The file system is divided in blocks of 256 bytes ( editable )

The first part of the file system is the superblock and have a size of 1024 bytes (which is 4 normal sized block (editable) [the count of the blocks gain +1 after each block, the block following the superblock is registered as 5])

An unused block can either be an inode or a datablock, there is no distinction.

Superblock:
The superblock contains the adresses of the file registered in our file system each adress is 40
bytes long (editable) and stock the full path of the file (for example /home/personal/file.t), the number of the block correspoding to the file's first inode and the date of the file creation on 8 bytes (editable) (in second since the january the first 1970, taking the 8 least significent bits). an adress in the superblock is then shaped as following: 
            
            /dir1/dir2/file.t(number_first_inode)date/

Inode:
An inode is a normal block which is divided in adresses of 8 bytes (editable). Each adress stock the number given to the block in which the data of the file are stocked. Each inode can point to 31 datablock. The last 8 bytes of the innode are reserved to point to the next innode of the file. If the file is to big to be fully stocked in 31 datablocks (7,9 kB) then the adresses of the datablocks are available in another inode which parameters are identic.

We organized our group for the project in the following way:

Gaëtan Plisson: functions create and write ; creation of the makefile

Alexandre Girardot: functions size and ls

Alban Sarrazin: functions read and delete (remove)

Each one of us compiled our own functions.

The debuggage has mainly been done by Gaëtan Plisson.

There were not many communication issues within the group from the beggining to the end, everyone knew what they have to do, and did their part. We had regular exchanges about the file system global parameters and values that will affect several functions, in order to have a working file system and that a given value have the same parameters and output in all the functions.

We did not have many blocking issues in this project, we advanced at a good and regular rythm and were never late on our timeline, excepting about the final commands format, that is not as specified an only executable because of macOS issues with compiling others files functions.
However, we still encountered some issues in this project: we sometimes have to use the sudo command to be able to run our functions.
Some functions create a compilation error in one of our computer (running on MACOS) and not in another computer (running on Linux).
