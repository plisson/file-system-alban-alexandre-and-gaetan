#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "./config.h"

long size(int fs, int inode,long long bytes){
  int fbytes = 0;
  for (size_t i = 0; i < DBSIZE-ADDRSIZEINODE; i+=ADDRSIZEINODE) {
    lseek(fs,inode*DBSIZE+i,SEEK_SET);
    char c[ADDRSIZEINODE];char s[1];
    for(int j=0;j<ADDRSIZEINODE;j+=1){
        read(fs,s,1);
        *(c+j)=*s;
    }
    //if (strncmp(c,"0000000/",ADDRSIZEINODE) == 0) {
    //  return bytes;
    //}
    int db = atoi(c);
    char buf[1];
    lseek(fs,inode*DBSIZE+i+2*ADDRSIZEINODE-1, SEEK_SET);
    read(fs,buf,1);
    if(*buf == '/'){
      char c2[1];
      lseek(fs,db*DBSIZE,SEEK_SET);
      read(fs,c2,1);
      while (*c2 != '\2') {
        fbytes++;
        read(fs,c2,1);
        if (fbytes==DBSIZE-1) {
          return bytes+DBSIZE;
        }
      }
      return bytes+fbytes;

    }else{
      fbytes+=DBSIZE;
    }
  }
  char nxtinode[ADDRSIZEINODE+1];
  lseek(fs,inode*DBSIZE+DBSIZE-ADDRSIZEINODE,SEEK_SET);
  read(fs,nxtinode,ADDRSIZEINODE);
  *(nxtinode+ADDRSIZEINODE) = '\0';
  //printf("%d %lld\n", atoi(nxtinode), bytes+fbytes);
  if (atoi(nxtinode)==0) {
    return bytes;
  }
  return size(fs,atoi(nxtinode),bytes+fbytes);
}
