#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "./config.h"

int main(int argc, char** argv) {
  int fs = open(*(argv+1),O_RDONLY);
  if (fs<0) {
    printf("%s\n","file access issues" );
    exit(0);
  }
  if (argc<3) {
    printf("%s\n", "usage: ./MYFS_ls <fs> '/dir/.../'");
    exit(0);
  }
  /**printf("%d\n", *(*(argv+2)+strlen(*(argv+2))-1)=='/');
  if (*(*(argv+1)+strlen(*(argv+2))-1)!='/') {
    printf("%s\n","path must be a directory one: /dir/.../dir2/" );
  }
  printf("%c\n", **(argv+2));
  if (**(argv+2)!='/') {
    printf("%s\n","path must be root relative" );
  }**/
  for (size_t i = 0; i < SIZESUPER*DBSIZE; i+=ADDRSIZESUPER) {
    lseek(fs,i,SEEK_SET);
    char* path = *(argv+2);
    char name[strlen(path)];
    read(fs, name, strlen(path));
    if (strncmp(path,name,strlen(path))==0) {
      lseek(fs,i,SEEK_SET);
      char Fname[ADDRSIZESUPER];
      char c[1];
      read(fs,c,1);
      int j = 0;
      while (*c!='(') {
        *(Fname+j)=*c;
        j++;
        read(fs,c,1);
      }
      *(Fname+j) = '\0';
      printf("%s\n", Fname);
    }
  }
  return 0;
}
