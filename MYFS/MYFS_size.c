#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "./config.h"

long findfileaddress(int fs, char* destpath){//get the adress of the file corresponding to the path given
    for (int i=0;i<SIZESUPER*DBSIZE;i+=ADDRSIZESUPER){//looking each super adress
        lseek(fs, i, SEEK_SET);
        char s[1];
        read(fs,s,1);
        if (*s != '\1') {//do nothing if free character
          char value[ADDRSIZESUPER];
          lseek(fs,i,SEEK_SET);
          int j=0;
          read(fs,s,1);
          while(*s!='('){//read the path
              *(value+j)=*s;
              j+=1;
              read(fs,s,1);
          }
          if((strncmp(value,destpath,j))==0){//compare the path with the address
              int j=0;
              char nbraddr[10];char s[1];
              read(fs,s,1);
              while(*s!=')'){//read the number of the datablock
                  *(nbraddr+j)=*s;
                  j+=1;
                  read(fs,s,1);
              }
              *(nbraddr+j+1) = '\0';
              int nbrinode=atoi(nbraddr);
              return nbrinode;//return it as integer
          }
        }
    }
    return 0;//error return
}
long size(int fs, int inode,long long bytes){
  int fbytes = 0;
  for (size_t i = 0; i < DBSIZE-ADDRSIZEINODE; i+=ADDRSIZEINODE) {//search at each address ( datablocks ) except the last one
    lseek(fs,inode*DBSIZE+i,SEEK_SET);
    char c[ADDRSIZEINODE];char s[1];
    for(int j=0;j<ADDRSIZEINODE;j+=1){//read the address
        read(fs,s,1);
        *(c+j)=*s;
    }
    int db = atoi(c);
    char buf[1];

    lseek(fs,inode*DBSIZE+i+2*ADDRSIZEINODE-1, SEEK_SET);
    read(fs,buf,1);
    if(*buf == '/'){//verify if the next address is a end address.
      char c2[1];
      lseek(fs,db*DBSIZE,SEEK_SET);//read this datablock
      read(fs,c2,1);
      while (*c2 != '\2') {//increment the written bytes by one until the end is reached or a terminating bytes is read
        fbytes++;
        read(fs,c2,1);
        if (fbytes==DBSIZE-1) {
          return bytes+DBSIZE;
        }
      }
      return bytes+fbytes;//add the corresponding bytes read

    }else{//else directly add one DBSIZE (because there is at last one DBSIZE more to read)
      fbytes+=DBSIZE;
    }
  }
  //Changing inodes
  char nxtinode[ADDRSIZEINODE+1];
  lseek(fs,inode*DBSIZE+DBSIZE-ADDRSIZEINODE,SEEK_SET);
  read(fs,nxtinode,ADDRSIZEINODE);
  *(nxtinode+ADDRSIZEINODE) = '\0';
  if (atoi(nxtinode)==0) {//read inode address on the last address of the actual inode
    return bytes;
  }
  return size(fs,atoi(nxtinode),bytes+fbytes);//recursive call with the next inode to read, and the actual bytes read
}
int main(int argc, char **argv) {
  if (argc < 3) {
    printf("usage: MYFS_size <file system path> <inputFile> [opt] -r [opt] -b/-k/-m/-g\n");
    exit(1);
  }
  int fs = open(*(argv+1),O_RDONLY);
  int div = 1;
  for (size_t i = 0; i < argc; i++) {
    if (strncmp(*(argv+i),"-g",2)==0) {
      div = 1000000000;
    } else if (strncmp(*(argv+i),"-m",2)==0) {
      div = 1000000;
    } else if (strncmp(*(argv+i),"-k",2)==0) {
      div = 1000;
    }
  }
  int r = 0;
  for (size_t i = 0; i < argc; i++) {
    if (strncmp(*(argv+i),"-r",2)==0) {
      r = 1;
    }
  }
  int stat = 0;
  for (size_t i = 0; i < argc; i++) {
    if (strncmp(*(argv+i),"-stat",5)==0) {
      stat = 1;
    }
  }
  if (r==1) {//if reccursive
    if (*(*(argv+2)+strlen(*(argv+2))-1)!='/') {
      printf("%s\n","this is not a directory. Use (./MYFS_size fs /dir/)  and not (.MYFS_size fs /dir)" );
      exit(0);
    }
    int exists=0;
    for (size_t i = 0; i < SIZESUPER*DBSIZE; i+=ADDRSIZESUPER) {//for each supernode address, look if the beginng of the path match and then read the size
      lseek(fs,i, SEEK_SET);
      char path[strlen(*(argv+2))];
      read(fs,path,strlen(*(argv+2)));
      if (strncmp(path,*(argv+2),strlen(*(argv+2)))==0) {
        exists++;
        char name[ADDRSIZESUPER];
        char s[1];
        lseek(fs,i,SEEK_SET);
        read(fs,s,1);
        int j = 0;
        while (*s!='(') {
          *(name+j) = *s;
          j++;
          read(fs,s,1);
        }
        *(name+j)='\0';
        long filesize = (size(fs,findfileaddress(fs,name),0)-1 );
        printf("%s : %ld ",name, filesize/div);
        if (stat==1) {
          printf("stats :%ld\n",(long) (ADDRSIZESUPER+filesize+DBSIZE-(filesize%DBSIZE)+(filesize+DBSIZE-(filesize%DBSIZE))*ADDRSIZEINODE/DBSIZE)/div);
        }else{
          printf("\n");
        }
      }
    }
    if (exists==0) {
      printf("%s\n","This directory does not exists." );
    }
  } else { //if no recussion.
    long filesize = (size(fs,findfileaddress(fs,*(argv+2)),0)-1);
    if (filesize<0) {
      printf("%s\n","this file does not exists" );
      exit(0);
    }
    printf("%s : %ld ",*(argv+2), filesize/div );
    if (stat==1) {
      printf("stats :%ld\n",(long) (ADDRSIZESUPER+filesize+DBSIZE-(filesize%DBSIZE)+(filesize+DBSIZE-(filesize%DBSIZE))*ADDRSIZEINODE/DBSIZE)/div);
    }else{
      printf("\n");
    }
  }



  return 0;
}
