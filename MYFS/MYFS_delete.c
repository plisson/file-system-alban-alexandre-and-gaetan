//
//  myfs_delete_sc.c
//
//
//  Created by Alban Sarrazin on 18/11/2022.
//

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "config.h"

int Delfileaddress(int fs, char* destpath){
    for (int i=0;i<SIZESUPER*DBSIZE;i+=ADDRSIZESUPER){
        char value[ADDRSIZESUPER];
        lseek(fs,i,SEEK_SET);
        int j=0;
        char s[1];
        read(fs,s,1);
        while(*s != '('){
            *(value+j)=*s;
            j+=1;
            read(fs,s,1);
        }
        if((strncmp(value,destpath,j))==0){
            int j=0;
            char nbraddr[10];
            read(fs,s,1);
            while(*s != ')'){
                *(nbraddr+j)=*s;
                j+=1;
                read(fs,s,1);
            }
            *(nbraddr+j+1) = '\0';
            int nbrinode=atoi(nbraddr);
            lseek(fs,i,SEEK_SET);
            write(fs,"\1",1);
            return nbrinode;
        }
    }
    return 0;
}


int Delinode(int fs, int nbrinode){
    int start=nbrinode*DBSIZE;
    for (int i=0;i<DBSIZE-ADDRSIZEINODE-1;i+=ADDRSIZEINODE){
        lseek(fs,start+i,SEEK_SET);
        char c[ADDRSIZEINODE];char s[1];
        for(int j=0;j<ADDRSIZEINODE;j+=1){
            read(fs,s,1);
            *(c+j)=*s;
        }
        if((strncmp(c,"0000000/",ADDRSIZEINODE))==0){  //indique si le nombre de datablock total a été atteint
            lseek(fs,start,SEEK_SET);
            write(fs,"\1",1);
            return 0;
        }
        int dbaddr;
        dbaddr=atoi(c);
        lseek(fs,dbaddr*DBSIZE,SEEK_SET);
        write(fs,"\1",1);

    }

    lseek(fs,start+DBSIZE-ADDRSIZEINODE,SEEK_SET);
    char nxtinode[ADDRSIZEINODE]; char s[1];
    for(int j=0;j<ADDRSIZEINODE;j+=1){
        read(fs,s,1);
        *(nxtinode+j)=*s;
    }
    lseek(fs, start,SEEK_SET);
    write(fs,"\1",1);
    if (atoi(nxtinode)<nbrinode) {
      return 0;
    }
    if((strcmp(nxtinode,"0000000/"))==0){
        return 0;
    }else{
        Delinode(fs,atoi(nxtinode));
    }
    return 0;
}


int MYFSdelete(int fs, char* destpath){
    int nbrinode=Delfileaddress(fs, destpath);
    return Delinode( fs, nbrinode);
}


int main(int argc, char** argv){
    MYFSdelete(open(*(argv+1),O_RDWR),*(argv+2));
    return 0;
}
