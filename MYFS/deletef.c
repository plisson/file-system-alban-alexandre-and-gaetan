
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "config.h"

int Delinode(int fs, int nbrinode){
    int start=nbrinode*DBSIZE;
    //printf("inode %d\n",nbrinode );
    for (int i=0;i<DBSIZE-ADDRSIZEINODE-1;i+=ADDRSIZEINODE){
      //printf("%d\n",i );
        lseek(fs,start+i,SEEK_SET);
        char c[ADDRSIZEINODE];char s[1];
        for(int j=0;j<ADDRSIZEINODE;j+=1){
            read(fs,s,1);
            *(c+j)=*s;
        }
        //printf("adresse %s\n",c );
        if((strncmp(c,"0000000/",ADDRSIZEINODE))==0){  //indique si le nombre de datablock total a été atteint
            lseek(fs,start,SEEK_SET);
            write(fs,"\1",1);
            return 0;
        }
        int dbaddr;
        dbaddr=atoi(c);
        lseek(fs,dbaddr*DBSIZE,SEEK_SET);
        //printf("%d\n",dbaddr );
        write(fs,"\1",1);

    }

    lseek(fs,start+DBSIZE-ADDRSIZEINODE,SEEK_SET);
    char nxtinode[ADDRSIZEINODE]; char s[1];
    for(int j=0;j<ADDRSIZEINODE;j+=1){
        read(fs,s,1);
        *(nxtinode+j)=*s;
    }
    lseek(fs, start,SEEK_SET);
    write(fs,"\1",1);
    if (atoi(nxtinode)==0) {
      return 0;
    }
    //printf("%s\n", "oui");
    if((strcmp(nxtinode,"0000000/"))==0){
        return 0;
    }else{
        return Delinode(fs,atoi(nxtinode));
    }
    return 0;
}
