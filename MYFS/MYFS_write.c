#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include "./config.h"

char* reverse(char* str, char* rts){//just a simple tool function to reverse a string.
  for (size_t i = 0; i <= strlen(str); i++) {
		*(rts+i) = *(str + strlen(str) - i -1);
  }
	return rts;
}

int findfreeaddress(int fs){
  int iposition = lseek(fs,0,SEEK_CUR);
  if (lseek(fs,0,SEEK_SET)==-1) {
   printf("Positioning issues: Error number: %d\n", errno);
  }
  for (size_t i = 0; i < SIZESUPER*DBSIZE; i+=ADDRSIZESUPER) {//look at each first bytes of addresses
    lseek(fs,i,SEEK_SET);
    char buf[1];
    if (read(fs, buf,1) == -1) {
      printf("%s\n", "error");
    }
    if (*buf=='\1'){//check if this address is free by checking if there is a free char in the begging of the super address
      lseek(fs,iposition,SEEK_SET);
      return i;
    }
  }
  printf("%s\n","Too many addresses allocated.");
  exit(1);//We are not dealing with the possibility of an overflow of the superblock. How sad.
}

int findfreedb(int fs){
  int iposition = lseek(fs,0,SEEK_CUR);
  int fsize = lseek(fs, 0, SEEK_END);
  if (lseek(fs,SIZESUPER*DBSIZE,SEEK_SET)==-1) {
   printf("Positioning issues: Error number: %d\n", errno);
  }
  for (size_t i = SIZESUPER*DBSIZE; i < fsize-DBSIZE; i+=DBSIZE) {//looking on each datablock if their is a free character and return his number
    lseek(fs,i,SEEK_SET);
    char buf[1];
    if (read(fs, buf,1) == -1) {
      printf("%s\n", "error");
    }
    if (*buf == '\1') {
      lseek(fs,iposition,SEEK_SET);
      return i;
    }

  }
  //if their is no more free db, delete oldest file
  long mindate = -1;
  int mindb = 0;
  long minpos = 0;
  for (size_t i = 0; i < SIZESUPER*DBSIZE; i+=ADDRSIZESUPER) {//get at every super address
    lseek(fs,i,SEEK_SET);
    char s[1];
    read(fs,s,1);
    if (*s!='\1') {//if the address is not free, read the number between parenthesis that is  the number of the datablock
      while(*s!='('){
        read(fs,s,1);
      }
      char number[12];
      read(fs,s,1);
      int j=0;
      while(*s!=')'){
        *(number+j)=*s;
        j++;
        read(fs,s,1);
      }
      *(number+j)='\0';
      int db = atoi(number);
      char datestr[DATESIGN+1];
      read(fs,s,1);
      j=0;
      while(*s!='/'){//read the date of creation ( of the file)
        *(datestr+j)=*s;
        j++;
        read(fs,s,1);
      }
      *(datestr+j)= '\0';
      int date = atoi(datestr);
      if (mindate==-1) {//initialisate comparation variables
        mindate=date;
        mindb=db;
        minpos=i;
      }
      if (date<mindate) {//edit them if their is a inferior date of creation
        mindate=date;
        mindb=db;
        minpos=i;
      }
    }
  }
  lseek(fs,minpos,SEEK_SET);
  write(fs,"\1",1);//mark the oldest file superaddress as free
  Delinode(fs,mindb);//mark inodes & datablocks as free. Alban's job.
  return findfreedb(fs);//Call back to get a freedb
}

int MYFSwrite(int fs, char* inputfile, char* destpath){
  int finput;
  if ((finput = open(inputfile, O_RDONLY))==-1){ //open the file
    printf("File access issues or file doesn't exist. Verify permissions. Error number: %d\n", errno);
    exit(errno);
  }
  if (lseek(fs,findfreeaddress(fs),SEEK_SET)==-1) {//go to a free superaddress
   printf("Positioning issues: Error number: %d\n", errno);
  }
  int inodeaddr = findfreedb(fs)/DBSIZE;
  time_t seconds;//get creation date in second until the 1 january 70s
  time(&seconds);
  seconds = (int) seconds;
  seconds = seconds%( (int) pow(10,DATESIGN));//just get DATESIGN least significant bytes, that's sufficient
  seconds = (int) seconds;
  if(strlen(destpath)+floor(log(inodeaddr)/log(10))+1+2+DATESIGN+1>ADDRSIZESUPER){//we do not deal to much long super addresses
    printf("%s\n","The address does not fit in ADDRSIZESUPER bytes" );
    exit(1);
  }

  char str[ADDRSIZEINODE+1];
  sprintf(str, "%d", inodeaddr);
  char* destpath2 = destpath;
  strcat(destpath2, "(");//writing the path and then the inode address in parenthesis in the superaddress
  if (write(fs, destpath2, strlen(destpath))==-1) {
    printf("%s\n","write error");
    exit(1);
  };
  if(write(fs, str, floor(log(inodeaddr)/log(10))+1) == -1){
    printf("A problem occured %d\n",errno );
    exit(1);
  }
  if (write(fs, ")",1)==-1) {
    printf("%s\n", "write error");
    exit(0);
  }
  char str2[DATESIGN];
  sprintf(str2,"%ld",seconds);
  if (write(fs,str2 ,DATESIGN)==-1) {//write the date of creation with end end character
    printf("%s\n", "write error");
    exit(1);
  }
  write(fs,"/",1);
  off_t fsize;
  fsize = lseek(finput, 0, SEEK_END);
  lseek(finput,0,SEEK_SET);
  int nbrDB = floor(fsize/DBSIZE)+1;//ok
  int nbrinode = floor(nbrDB*ADDRSIZEINODE/DBSIZE)+1+1;
  int actualinode = inodeaddr;

  long long bytesWritten =0;
  lseek(fs,actualinode*DBSIZE,SEEK_SET);
  write(fs,"0",1);
  for (int i=0;i<=nbrinode;i+=1){ //for each inode needed
    for(int a=0; a<DBSIZE-ADDRSIZEINODE-1; a+=ADDRSIZEINODE){ //write all addresses of this inodes and corresponding data on datablocks
      int db = findfreedb(fs)/DBSIZE;
      char str[(int) floor(log(db)/log(10))+1];
      int initlen = (int) floor(log(db)/log(10))+1;
      sprintf(str,"%d",db);
      char buf[ADDRSIZEINODE];
      for (size_t c = 0; c < ADDRSIZEINODE; c++) {
        *(buf+c) = '0';
      }
      lseek(fs,actualinode*DBSIZE+a,SEEK_SET);
      write(fs, buf, ADDRSIZEINODE);

      for(int c = 0; c <initlen; c++){//write the address in the inode ( on the right)
        lseek(fs,actualinode*DBSIZE+a+ADDRSIZEINODE-c-1, SEEK_SET);
        write(fs,str+initlen-c-1,1);
      }
      lseek(fs,db*DBSIZE,SEEK_SET);//got to corresponding datablock
      for (int c = 0; c < DBSIZE; c++) {//writing all bytes until the entire set of bytes is written and write an end character ( except is the bytes is already on the last place)
        char s[1];
        read(finput,s,1);
        write(fs,s,1);
        if ((bytesWritten++)==fsize) {
          if (c<DBSIZE-1) {
            write(fs,"\2",1);
          }
          //Then write on the next address an address end mark
          lseek(fs,actualinode*DBSIZE+a+ADDRSIZEINODE,SEEK_SET);
          char buf[ADDRSIZEINODE];
          for (int c = 0; c < ADDRSIZEINODE-1; c++) {
            *(buf+c)= '0';
          }
          *(buf+ADDRSIZEINODE-1) = '/';
          write(fs,buf,ADDRSIZEINODE);

          return 0;
          exit(0);
        }
        }
      lseek(fs,actualinode*DBSIZE+a,SEEK_SET);
    }
    //if needing more inodes
    int lastinode = actualinode;
    actualinode = findfreedb(fs)/DBSIZE;//find an other inode
    lseek(fs,actualinode*DBSIZE,SEEK_SET);
    write(fs,"0",1);//mark as used

    char str[(int) floor(log(actualinode)/log(10))+1];
    int initlen = floor(log(actualinode)/log(10))+1;
    sprintf(str,"%d",actualinode);
    char buf[ADDRSIZEINODE];
    for (size_t c = 0; c < ADDRSIZEINODE; c++) {
      *(buf+c) = '0';
    }
    lseek(fs,(lastinode+1)*DBSIZE-ADDRSIZEINODE-1,SEEK_SET);
    write(fs, buf, ADDRSIZEINODE);//write the new inode address at the end of the previous one
    for(int c = 0; c <initlen; c++){
      lseek(fs,(lastinode+1)*DBSIZE-c-1, SEEK_SET);
      write(fs,str+initlen-c-1,1);
    }
  }
  return 1;
}

int main(int argc, char *argv[]) {
  if (argc < 3) {
    printf("usage: MYFS_create <file system path> <inputFile> <destinationFile>\n");
    exit(1);
  }
  int fs;
  if ((fs = open(*(argv+1), O_RDWR))==-1){
    printf("File access issues. Verify permissions. Error number: %d\n", errno);
    exit(errno);
  }
  if (**(argv+3)!='/') {
    printf("%s\n","path must be root relative : /dir/.../file" );
    exit(0);
  }
  int pass = 0;
  for (size_t i = 0; i < SIZESUPER*DBSIZE; i+=ADDRSIZESUPER) {//verify in each file that no one has the same name
    char path[ADDRSIZESUPER];
    char s[1];
    lseek(fs,i,SEEK_SET);
    read(fs,s,1);
    if (*s!='\1') {
      int j=0;
      while (*s!='(') {
        *(path+j)=*s;
        j++;
        read(fs,s,1);
      }
      *(path+j)='\1';
      if (strncmp(*(argv+3),path,strlen(*(argv+3)))==0) {
        pass = 1;
      }
    }

  }
  if (pass==1) {
    printf("%s\n","This file already exists" );
    exit(0);
  }
  return MYFSwrite(fs,*(argv+2),*(argv+3));
  return 0;
}
