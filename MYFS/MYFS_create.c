#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "./config.h"


int create(int size, int fs){
  for (size_t i = ADDRSIZESUPER; i < SIZESUPER*DBSIZE; i+=ADDRSIZESUPER) {//marking a free character on each first bytes of adresses
    if ((write(fs,"\1", 1)==-1)) {
      printf("Writing issues: Error number: %d\n", errno);
    }
    if (lseek(fs,i,SEEK_SET)==-1) {
     printf("Positioning issues?: Error number: %d\n", errno);
    }
  }
  for (size_t i = SIZESUPER*DBSIZE; i < size-DBSIZE; i+=DBSIZE) {//marking a free character on each first bytes of datablocks
    if ((write(fs,"\1", 1)==-1)) {
      printf("Writing issues: Error number: %d\n", errno);
    }
    if (lseek(fs,i,SEEK_SET)==-1) {
     printf("Positioning issues: Error number: %d\n", errno);
    }
  }

  if ((lseek(fs,size,SEEK_SET))==-1) {
   printf("Positioning issues: Error number: %d\n", size);
  }
  if ((write(fs,"\0", 1)==-1)) { //marking a end character in the end of the file system to signify the lenght of the file system
    printf("Writing issues: Error number: %d\n", errno);
  }


  printf("File System Created\n");
  return 0;
}



int main(int argc, char** argv){
  if (argc < 2) {
    printf("usage: MYFS_create <file system path> <name> <size>\n");
    exit(1);
  }
  int fs;
  if ((fs = open(*(argv+1), O_RDWR | O_CREAT))==-1){
    printf("File access issues. Verify permissions. Error number: %d\n", errno);
    exit(errno);
  }
  int size = atoi(*(argv+2))*1000000;
  exit(create(size,fs));
}
