//
//  myfs_read_sc.c
//
//
//  Created by Alban Sarrazin on 18/11/2022.
//
//probleme de lecture apres reecriture auto
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "./config.h"


//function findfileaddress, the objective of this function is to obtain the address of the first inode of the file we want to read. To do so the function search the file address in the superblock in which is added after the path of the file a number between parenthesis. This number gives the system the adress of the inode it is looking for.
int findfileaddress(int fs, char* destpath){
    for (int i=0;i<SIZESUPER*DBSIZE;i+=ADDRSIZESUPER){
        lseek(fs, i, SEEK_SET);
        char s[1];
        read(fs,s,1);
        if (*s != '\1') { //looks if the adress if free, if it is then the program will look at the next adress in the superblock
          char value[ADDRSIZESUPER];
          lseek(fs,i,SEEK_SET); //the program reads the path written in the adress it is looking into
          int j=0;
          read(fs,s,1);
          while(*s!='('){
              *(value+j)=*s;
              j+=1;
              read(fs,s,1);
          }
          if((strncmp(value,destpath,j))==0){ // The program looks if the path of the adress corresponds to the given path. If it does not correspond, then the program look at the next adress
              int j=0;
              char nbraddr[10];char s[1];
              read(fs,s,1);
              while(*s!=')'){ //If the path of the selected adress corresponds to the given path, the program reads the number representing the file's first inodee
                  *(nbraddr+j)=*s;
                  j+=1;
                  read(fs,s,1);
              }
              *(nbraddr+j+1) = '\0';
              int nbrinode=atoi(nbraddr);
              return nbrinode;//The program returns the number of the first inode of the file we want to read, as intended.
          }
        }
    }
    return -1;
}//ok


//Function readinode, the objective of this function is to obtain the adresses and the data of the datablocks that are stocking the data corresponding to the file we want to read.
char* readinode(int fs, int nbrinode, char* output){
  ;
    long start=nbrinode*DBSIZE;
    for (int i=0;i<DBSIZE-ADDRSIZEINODE;i+=ADDRSIZEINODE){
        //printf("%d ",i );
        lseek(fs,start+i,SEEK_SET);
        char c[ADDRSIZEINODE];char s[1];
        for(int j=0;j<ADDRSIZEINODE;j+=1){
            read(fs,s,1);
            *(c+j)=*s;
        }
        if((strncmp(c,"0000000/",ADDRSIZEINODE))==0){  //indique si le nombre de datablock total a été atteint
            printf("%s\n","la" );

            return output;
        }
        char buf[1];
        long dbaddr=atoi(c);
        lseek(fs,dbaddr*DBSIZE,SEEK_SET);
        read(fs,buf,1);
        int j=0;
        while (j<DBSIZE) {
          if (*buf=='\2') {
            return output;
          }
          strncat(output, buf,1);
          j++;
          read(fs,buf,1);
        }
    }
    lseek(fs,start+DBSIZE-ADDRSIZEINODE,SEEK_SET);
    char nxtinode[ADDRSIZEINODE];//char s[1];
    read(fs,nxtinode,ADDRSIZEINODE);

    if(atoi(nxtinode)==0){
        return output;
    }else{
        return readinode(fs,atoi(nxtinode), output);
    }
    return 0;
}

char* initreadinode(int fs, int nbrinode){
    char* output = malloc(size(fs,nbrinode,0)+1);//integrer avec size
    output = readinode(fs,nbrinode,output);
    *(output+size(fs,nbrinode,0))='\0';
    return output;
}

char* MYFSread(int fs, char* destpath){
    int nbrinode=findfileaddress(fs,destpath);
    if (nbrinode<0) {
      printf("This file does not exist.\n" );
      exit(1);
      return 0;
    }
    return initreadinode(fs, nbrinode);
}

int main(int argc, char** argv){
    printf("%s\n",MYFSread(open(*(argv+1), O_RDWR),*(argv+2)));
    return 0;
}
